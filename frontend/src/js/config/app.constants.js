const AppConstants = {
  //api: 'https://conduit.productionready.io/api',
  api: 'http://localhost:8080/api',
  
  jwtKey: 'jwtToken',
  appName: 'JoinUp',
};

export default AppConstants;
