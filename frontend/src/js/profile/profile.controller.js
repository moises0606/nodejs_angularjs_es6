class ProfileCtrl {
    constructor(profile, User, $window,AppConstants,$state,Toaster, $scope, Newevents) {
        'ngInject';


        this._$state = $state;
        this._toaster = Toaster;
        this._$scope = $scope;
        this._Newevents = Newevents;
        this.profile = profile;
        $scope.states = {}
        var vm = this;
        $scope.events = true;
        $scope.noevents = false;
        $scope.states.activeItem = 'myevents';
        $scope.btnmevnt = true;
        $scope.btnfollow = false;
        
        if (profile.myevents.length == 0) {
            $scope.events = false;
            $scope.noevents = true;
        }

        if (User.current) {
            this.isUser = (User.current.username === profile.username);
            this.events = profile.myevents;
        } else {
            this.isUser = false;
        }

        $scope.openDetailsprof = function() {
            $state.go('app.detailevent', { id: this.event._id });
        };

        $scope.changeTo = function(type) {
            $scope.noevents = false;
            $scope.events = true;
            if (type == "myevents"){
                $scope.states.activeItem = 'myevents';
                vm.events = profile.myevents;
                $scope.btnfollow = false;
                $scope.btnmevnt = true;
                if (profile.myevents.length == 0) {
                    $scope.events = false;
                    $scope.noevents = true;
                }
            }else if (type==="follow") {
                $scope.states.activeItem = 'follow'
                vm.events = profile.followevents;
                $scope.btnmevnt = false;
                $scope.btnfollow = true;
                if (profile.followevents.length == 0) {
                    $scope.events = false;
                    $scope.noevents = true;
                }
            }
        };

        $scope.remove = function(mevent) { 
            vm._Newevents.remove(mevent).then((data) => {
                if (data.data == true)
                    vm._toaster.showToaster('success','Evento eliminado');    
                profile.myevents.filter((event, index) => {
                    if (event._id === mevent._id) {
                        console.log(mevent._id);
                        profile.myevents.splice(index,1);
                        vm.events = profile.myevents;
                        if (profile.myevents.length == 0) {
                            $scope.events = false;
                            $scope.noevents = true;
                        }
                    }
                })
            });
        }

        $scope.unfollow = function(fevent) { 
            User.userUnsuscribe(fevent._id).then((data) => {
                if (data)
                    vm._toaster.showToaster('success','Evento desuscrito');    
                profile.followevents.filter((event, index) => {
                    if (event._id === fevent._id) {
                        console.log(fevent._id);
                        profile.followevents.splice(index,1);
                        vm.events = profile.followevents;
                        if (profile.followevents.length == 0) {
                            $scope.events = false;
                            $scope.noevents = true;
                        }
                    }
                })
            });
            
        }

    }
}


   


export default ProfileCtrl;
