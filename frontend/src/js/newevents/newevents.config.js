

function NeweventsConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.newevents', {
      url: '/',
      controller: 'NeweventsCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'newevents/newevents.html',
      title: 'Newevents',
      resolve:{
        user: function(User) {
          return User.getUser();
        }
       

      }
    });
  
  };
  
  export default NeweventsConfig;
  