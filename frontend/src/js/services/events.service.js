export default class Events {
    constructor(JWT, AppConstants, $http, $q,  NgMap) {
      'ngInject';
  
      this._AppConstants = AppConstants;
      this._$http = $http;
      this._$q = $q;
    }
  
    getAll() {
        return this._$http({
        url: this._AppConstants.api + '/events',
        method: 'GET',
      }).then((res) => res.data.events);
    }

    getendvents() {
      return this._$http({
        url: this._AppConstants.api + '/home/endevent',
        method: 'GET',
      }).then((res) => res.data.endevent);
        //  console.log(res.data.endevent);
      
    }


    getDeportes() {
      return this._$http({
        url: this._AppConstants.api + '/deportes',
        method: 'GET',
      }).then((res) => res.data.deportes);
    }
    
    getEvent(id) {
      let deferred = this._$q.defer();

      this._$http({
        url: this._AppConstants.api + '/events/'+ id,
        method: 'GET',
      }).then((res) => deferred.resolve(res.data.events));

      return deferred.promise;
    }

    gettype(type) {
      let deferred = this._$q.defer();

      this._$http({
        url: this._AppConstants.api + '/events/ftype/'+type,
        method: 'GET',
      }).then((res) => deferred.resolve(res.data.events));

      return deferred.promise;
    }

   geolocate(){
    return this.getAll().then((eventos) => {
      let ordenados = [];

      const promise = new Promise(function(resolve, reject) {
        /* Recoger localización del usuario, si el usuario deniega se devuelven todos los eventos
        sin ningun orden */
        navigator.geolocation.getCurrentPosition(success, error);
        function success(position){

          /* De cada uno de los eventos hace de la ubicacion un numero que cuanto mas cerca de 
          0 esté mas cerca de nosotros estarà el evento */
          eventos.forEach(element => {
            let latitudeUser = position.coords.latitude;
            let longitudeUser = position.coords.longitude;
            let latitudEvent = element.latitud;
            let longitudEvent = element.longitud;
            let total;

            if (latitudeUser > latitudEvent){
              if(latitudEvent > 0){
                latitudEvent = latitudEvent-latitudEvent*2;
              }
              latitudeUser = latitudeUser-latitudEvent;
            }else{
              if(latitudeUser > 0){
                latitudeUser = latitudeUser-latitudeUser*2;
              }
              latitudeUser = latitudEvent-latitudeUser;
            }

            if (longitudeUser > longitudEvent){
              if(longitudEvent > 0){
                longitudEvent = longitudEvent-longitudEvent*2;
              }
              longitudeUser = longitudeUser-longitudEvent;
            }else{
              if(longitudeUser > 0){
                longitudeUser = longitudeUser-longitudeUser*2;
              }
              longitudeUser = longitudEvent-longitudeUser;
            }

            total = latitudeUser+longitudeUser;
            // ordenados.push({element, "puntuacion" : total});
            ordenados.push({element, "puntuacion" : total});
          });
          /* Se ordenan los eventos según lo cerca que estàn*/
          ordenados = ordenados.sort(function(a, b){return a.puntuacion - b.puntuacion});

          /* Se guarda en event todos los eventos ordenados */
          let event = [];
          ordenados.forEach(element => {
            event.push(element.element);
          });
          resolve(event);
        }
        function error(error){
          if (error.code == error.PERMISSION_DENIED){
            resolve(eventos);
          }
        };
      });
      return promise
    })
  }
}// end brackets 