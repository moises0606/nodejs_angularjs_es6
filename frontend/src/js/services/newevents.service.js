export default class Newevents {
    constructor(JWT, AppConstants, $http, $q,  NgMap) {
      'ngInject';
  
      this._AppConstants = AppConstants;
      this._$http = $http;
      this._$q = $q;
      this._JWT = JWT;
    }

  
    save(Newevent) {
      console.log(Newevent);
      //console.log(this._JWT.get());
      if (Newevent) {
        return this._$http({
          url: this._AppConstants.api + '/newevent',
          method: 'POST',
          headers: {
            Authorization: 'Token ' + this._JWT.get()
          },
          data: {
            newevent: Newevent
          }
        }).then((newevent) => {
          //console.log(newevent);
          return newevent;
        })
      }
    }

    remove(event) {
      if (event) {
        return this._$http({
          url: this._AppConstants.api + '/newevent/removeevent',
          method: 'PUT',
          headers: {
            Authorization: 'Token ' + this._JWT.get()
          },
          data: {
            event: event
          }
        }).then((event) => {
          return event;
        })
      }
    }


}// end brackets 