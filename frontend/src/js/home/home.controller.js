class HomeCtrl {
  constructor(User, AppConstants, $scope, Events,profile,$timeout) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this.Events = [];
    this.profile = [];
    var vm = this;
    $scope.followevents= false;
    $scope.Endevent =[];
    vm.profile = profile ;
    $scope.profile= vm.profile;


     if (profile  != undefined){

        if (vm.profile.followevents.length !=0){
            $scope.followevents= true;
        }
     }    

    Events.getendvents().then((endevent)=>{
    // this.endeventLoaded = true;
   
     $scope.Endevent = endevent
     vm.endevent = endevent ;
     $scope.endevent= vm.endevent;
     console.log(vm.endevent);
     console.log($scope.endevent);
    });

    Events.getDeportes().then((deportes) => {
      vm.deportes = deportes[0].categories["0"];
      this.deportesLoaded = true;
      $scope.deportes = vm.deportes;
     
    });

    // Set current list to either feed or all, depending on auth status.
    this.listConfig = {
      type: User.current ? 'feed' : 'all'
    };

  }

  changeList(newList) {
    this._$scope.$broadcast('setListTo', newList);
  }

}

export default HomeCtrl;
