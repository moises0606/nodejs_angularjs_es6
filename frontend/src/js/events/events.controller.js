class EventsCtrl {
    constructor(User, Events, eventos, AppConstants, $scope, $timeout, NgMap, $stateParams, $state, $filter) {
      'ngInject';
 
this.positions = [];
this.showMap = true;
this.lng=0;
this.lat =0;
this._$scope = $scope;
this.Events = [];
this.msgshowmap="Ocultar Mapa";
$scope.AllEvents = [];

let vm = this;
NgMap.getMap().then(function(map) {
  vm.map = map;
});

/* Se debe encontrar la manera de eliminar esta peticion */
Events.getAll().then((Events) => {
    //if (eventos) Events=eventos;

    $scope.AllEvents = eventos;

    /* Abans hi habia Events = Events.slice(0, 5) */

    let primeroseventos = eventos.slice(0, 5);
    vm.Events = primeroseventos;
    this.eventsLoaded = true;
    
    /* Se comprueba que el limite y los inscritos no sea igual, si lo és se pinta completo! */
    eventos.forEach(element => {
      if(element.inscritos.length===element.limite) $scope.completar=true;      
    });
    
    $scope.getMarkers();
});

let map = document.getElementById("mapEvents");

/* Esta funcion es llamada por un lisener que esta abajo de ella y lo que hace
es recoger la altura donde estan los eventos y el footer y mueve arriba o abajo dependiendo si
el footer esta cerca o no de él */
function runOnScroll(){

    function offset(el) {
      var rect = el.getBoundingClientRect(),
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      return { top: rect.top + scrollTop}
    }
    
    let foter = document.getElementById("footer").clientHeight;
    let web = document.getElementById("main").clientHeight;
    let div = document.querySelector('#foter');
    let divOffset = offset(div);

    let a = web-foter*2;

    if (divOffset.top < a){
      if (map.classList.contains("moveupMap"))
        map.classList.toggle("moveupMap")
    }else{
      if (!map.classList.contains("moveupMap")){
        map.classList.add("moveupMap");
      }
    }
}

window.addEventListener("scroll", runOnScroll);

$scope.myPagingFunction = function() {
    Events = $scope.AllEvents.slice(0, Events.length + 5);

    let Eventos = $scope.AllEvents.slice(0, eventos.length + 5);

    vm.Events = Eventos;
    this.eventsLoaded = true;
    
    Eventos.forEach(element => {
      if(element.inscritos.length===element.limite) $scope.completar=true;      
    });

};

$scope.openDetails = function() {
  $state.go('app.detailevent', { id: vm.eventItem.id });
};

/* Cuando se pincha desde el mapa se comprueba cual es y se muestran los detalles
en un infowondow en el mapa */
$scope.openDetailsmarker = function(e, eventItem) {
  
  let Eventos = getFilteredEvents(vm.Events,vm.filtro);
  let id;
  let deporte;
  let imagen;
  let place;
  let fecha;
  let hour;
  let name;

  Eventos.forEach(element => {
    if(element._id===eventItem.id){
      id = element['_id'];
      deporte = element['deporte'];
      name = element['name'];
      imagen = element['img'];
      place = element['place'];
      fecha = element['fecha'];
      hour = element['hour'];
    }
  });

  let pos = {
    id: id,
    name: name,
    place :place,
    deporte:deporte,
    hour:hour,
    fecha:fecha,
    imagen: imagen,
    }

  vm.eventItem = pos;
  vm.map.showInfoWindow('myInfoWindow', id);
};

/* Despues de mostrar los detalles en el mapa, si se pincha en los detalles
te redirecciona a los detalles del evento */
$scope.openDetailsclickinfomarkers = function() {
  $state.go('app.detailevent', { id: vm.eventItem.id });
};

/* Recoge todos los eventos y muestra en el mapa su localización según
los eventos que se hayan cargado apareceràn en el mapa */
$scope.getMarkers = function() {
  vm.positions = [];
  let eventosConGeo = getFilteredEvents(vm.Events,vm.filtro);

  if(eventosConGeo.length > 0){
    let centerLat = 0;
    let centerLong = 0;
    eventosConGeo.forEach(element => {

      let latitude = element['latitud'];
      let longitude = element['longitud'];
      centerLat += parseFloat(latitude);
      centerLong += parseFloat(longitude);

      let id = element['_id'];
      let name = element['name'];
      let type = element['type'];
      let imagen = element['img'];
      let pos = {
        id: id,
        name: name,
        type: type,
        imagen: imagen,
        pos:
          [parseFloat(latitude),
          parseFloat(longitude)]
        }
      vm.positions.push(pos);
    });
    centerLat = parseFloat(centerLat) / eventosConGeo.length;
    centerLong = parseFloat(centerLong) / eventosConGeo.length;
    vm.lat  = centerLat;
    vm.lng = centerLong;
  }
}

/* Se esconde o muestra el mapa */
$scope.ShowHideMap = () => {
  if (this.showMap){
    if (!map.classList.contains("ocultar"))
      map.classList.add("ocultar");
    this.showMap=false;
    this.msgshowmap="Muestra Mapa";
  }else{
    if (map.classList.contains("ocultar"))
      map.classList.toggle("ocultar")
    this.showMap=true;
    this.msgshowmap="Ocultar Mapa";
    
  }
}

$scope.showDetailsOnHover = function() {
  let id = this.event['_id'];
  let deporte = this.event['deporte'];
  let imagen = this.event['img'];
  let place = this.event['place'];
  let fecha = this.event['fecha'];
  let hour = this.event['hour'];
  let name = this.event['name'];
  let pos = {
    id: id,
    name: name,
    place :place,
    deporte:deporte,
    hour:hour,
    fecha:fecha,
    imagen: imagen,
    }
  vm.eventItem = pos;
  vm.map.showInfoWindow('myInfoWindow', id);
}

function getFilteredEvents(eventos,filtro) {
  return $filter('filter')(eventos,filtro);
}

};

} //end brackets

export default EventsCtrl;