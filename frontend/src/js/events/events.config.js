function EventsConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.events', {
      url: '/events/:type',
      controller: 'EventsCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'events/events.html',
      title: 'Events',
      resolve: {
        eventos: function(Events, $state, $stateParams) {
          if ($stateParams.type){
            return Events.gettype($stateParams.type).then(
              (Events) => Events,
              (err) => $state.go('app.home')
            )
          }else {
            const promise = new Promise(function(resolve, reject) {
              Events.geolocate().then(
                (eventos)=> resolve(eventos)
              )
            });
            return promise
          }

        }
      }
    })

.state('app.detailevent', {
  url: '/event/:id',
  controller: 'DetailEventCtrl',
  controllerAs: '$ctrl',
  templateUrl: 'events/detailevent.html',
  title: 'Detail Event',
  resolve: {
    event: (Events, $state, $stateParams) => {
      return Events.getEvent($stateParams.id)
      .then(
        (function (MEvent) {
            return Events.gettype(MEvent.deporte)
            .then(
                (Events) => {
                  MEvent.mevents = Events;
                  return MEvent;
                }
              );
        }),
        (err) => $state.go('app.home')
      )
    },
    organizadorEvento: (User, Events, $stateParams) =>{
      return User.verifyAuth().then((value)=>{
        if(value){
          return User.getUser().then((user)=>{
            return Events.getEvent($stateParams.id).then((event)=>{
              if (event.organizador === user.id){
                /* Esto indica en el details si el boton de suscribirse esta visible o no,
                si es el organizador retornamos false */
                return false;
              }else{
                return true;
              }
            })
          })
        }
      })
    }
  }
})

};
export default EventsConfig;
