import angular from 'angular';
import ngInfiniteScroll from 'ng-infinite-scroll';

// Create the module where our functionality can attach to
let eventsModule = angular.module('app.events', [ngInfiniteScroll]);

// Include our UI-Router config settings
import EventsConfig from './events.config';
eventsModule.config(EventsConfig);


// Controllers
import EventsCtrl from './events.controller';
eventsModule.controller('EventsCtrl', EventsCtrl);

import DetailEventCtrl from './detailevent.controller';
eventsModule.controller('DetailEventCtrl', DetailEventCtrl);



export default eventsModule;
