INTERFAZ--------------------------
	Cuando accedemos a la Aplicacion Web, podremos observar que en la parte de arriba
	esta el menu, desde aquí podremos acceder a los diferentes modulos dependiendo de que tipo
	de usuario seamos (user, guest)
	Si somos guest, podremos acceder al HOME, EVENTS, CONTACT, SIGN IN, SIGN UP
	Si somos user, podremos acceder a lo mismo que el usuario guest pero podremos acceder al NEW EVENT, SETTINGS, PROFILE.
----------------------------------

USUARIO NORMAL = USUARIO LOGGEADO
USUARIO GUEST = USUARIO SIN LOGUEAR

------------------------------HOME
	En el home podemos ver unas categorias que cuando pulsamos en alguna de ellas, nos redirige 
	al events y nos muestra una lista de los eventos que esten en esta categoria
		(USUARIO GUEST)
		Podemos ver que en la parte superior de la pagina nos mostrará una seleccion de los 
		eventos mas destacados en funcion de la fecha
		
		(USUARIO NORMAL)
		En el home podremos ver algunos de los eventos a los que estamos suscritos, ya que la 
		aplicacion web nos recordará a que eventos debemos de ir preparandonos
		Si no estamos suscritos a ningun evento, no nos mostrará nada

		--------
		Cuando pulsamos en cualquier evento (destacado o suscrito) del home (ya sea como guest o como usuario)
		nos redigirá al details del propio evento, que es donde podremos ver toda la información del
		evento
----------------------------------

----------------------------EVENTS
	Cuando nos situamos en el modulo de events, nos solicitará acceso a la ubicacion para poder
	personalizar la experiencia de usuario y mostrar de forma geolocalizada los eventos.
	Si denegamos la geolocalización el usuario perderá experiencia pero podrá continuar utilizando 
	la aplicacion web ya que se le mostrarán los eventos de forma desordenada.

	Como podremos observar, se nos listarán los eventos. Como usuarios podremos pulsar sobre un 
	evento y nos redigirá al details del mismo evento.

	También podremos observar a la parte derecha un mapa, en el cual se geolocalizaran los eventos 
	para que el usuario vea donde se producirán estos. El usuario siempre que quiera podrá deshabilitar
	el mapa i ver solo los eventos, pero no vera en un mapa donde se realizarán.

	Cuando pulsamos en la ubicacion de un evento dentro del mapa, podremos observar que se abrirá
	un infowindow en el cual podremos observar una pequeña información del evento. Esto también se 
	activará cuando pasemos por encima de un evento (del listado)
		-------------------DETAILS
			En el details podremos ver que usuario, ha sido el organizador del evento, los usuarios que
			se han suscrito (se verán en el listado desplegable de inscritos), toda la informacion del evento
			y mas abajo podremos observar un mapa donde el evento está geolocalizado
			
			Además debajo del mapa podremos ver (en caso de que haya mas) un maximo de 5 eventos 
			relacionados con este mismo evento (la relación es el deporte del evento), cuando pulsamos en 
			alguno de estos eventos nos redigirá al details del evento seleccionado
			
			Todos los usuarios mostrados en el details se guardan en la BBDD por id y se muestra el username de cada uno
			
			(USUARIO NORMAL)
			Abajo de la imagen, podremos ver (si somos usuario normal) un botón para suscribirnos al evento,
			cuando nos suscribimos, el listado (de inscritos) se actualiza automaticamente y el botón pasará a
			ser de desuscribirse, que este realizará la acción contraria a la realizada previamente, es decir nos 
			desuscribirá del evento actualizando el listado automaticamente
		--------------------------
----------------------------------

-------------------------NEW EVENT
	Si accedemos a este modulo podremos ver un formulario que rellenandolo el usuario podrá crear un 
	evento para que los otros usuarios se puedan suscribir para realizar esta actividad

	Cuando el usuario crea el evento se guardará automaticamente su id en organizador y en inscritos
	del evento creado anteriormente y se redigirá al home mostrando un tastr de confirmación
----------------------------------

--------------------------SETTINGS
	En settings el usuario podrá ver su informacion personal el cual podrá cambiar
	Cuando se actualiza la información se muestra un toastr de confirmación
----------------------------------

---------------------------PROFILE
	Cuando el usuario accede al profile este podrá ver su informacion personal en la parte superior
	de la pagina, también tendrá un boton donde podrá ser redirigido al settings por si este quiere
	actualizar su información personal

	Mas abajo el usuario podrá ver dos apartados que se mostrarán o se ocultarán dependiendo que
	apartado a solicitado ver el usuario. Inicialmente se muestran los eventos que el usuario
	ha creado, desde aquí el usuario podrá eliminar los eventos que el mismo ha creado y este evento
	se eliminara de la BBDD y se desuscribirá automaticamente a todos los usuarios que se habian
	suscrito a este mismo evento
	Si el usuario desea ver los eventos que sigue (que se ha suscrito), puede hacer clic en esta 
	ventana y se le mostrarán los eventos a los que este usuario se ha suscrito, desde aquí el 
	usuario tiene también la opción de desuscribirse del evento

	Cuando el usuario elimina o se desuscribe de un evento, la lista se actualiza automaticamente
	y se muestra un toastr de confirmación
	También el usuario si lo desea podrá acceder al details del evento que seleccione ya que 
	se le redigirá al hacer clic
----------------------------------

-----------------------------LOGIN
SIGN IN
	Desde el sign in podemos loggearnos, introducimos el email, y contraseña
	Si se ha realizado con exito, mostrará un toastr y redireccionara al Home, en caso
	contrario se mostrará un toastr de error

SIGN IN SOCIAL
	Podremos loggearnos clicando en la red social que deseamos (desde el menu)

SIGN UP
	Desde el sign up nos podremos registrar, siempre se realizará como usuario normal (user)
	Si se ha realizado con exito, mostrará un toastr

LOGOUT
	Cuando pulsamos logout, desde el settings, se destruirá la sesion del usuario.
----------------------------------

---------------------------CONTACT
	Desde este modulo, el usuario podrá enviar un email para poder contactar con el administrador
	de la pagina web
	Y se redigirá al home con toastr de confirmación
----------------------------------

---------------------------FOOTER
	Desde el Footer podremos ver informacion sobre la empresa que en este caso es Lorem Ipsum y si el usuario desea mas información al pulsar Read More lo redirigiremos a Contact por otra parte también tenemos los botones para llevarnos a las redes sociales en este caso los botones no tienen ninguna accion asociada.
	También podremos encontrar la direccion de nuestra sede y si queremos mandar una consulta, nos redigira a Contact
----------------------------------