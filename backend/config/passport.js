var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var GitHubStrategy = require('passport-github2').Strategy;
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var socialKeys = require('../credentials/credentials.json');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  // console.log(`id: ${id}`);
  User.findById(id)
    .then(user => {
      done(null, user);
    })
    .catch(error => {
      console.log(`Error: ${error}`);
    });
});

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]'
}, function(email, password, done) {
  User.findOne({email: email}).then(function(user){
    if(!user || !user.validPassword(password)){
      return done(null, false, {errors: {'email or password': 'is invalid'}});
    }
    return done(null, user);
  }).catch(done);
}));


passport.use(new GoogleStrategy({
  clientID: socialKeys.GOOGLEPLUS_CLIENT_ID,
  clientSecret: socialKeys.GOOGLEPLUS_CLIENT_SECRET,
  callbackURL: socialKeys.GOOGLEPLUS_CALLBACK,
  passReqToCallback: true
  },
  function(request, accessToken, refreshToken, profile, done) {
    //console.log(profile);
    User.findOne({ 'idsocial' : profile.id }, function(err, user) {
        if (err)
          return done(err);

        // if the user is found then log them in
        if (user) {
            console.log('USER EXISTS');
            return done(null, user);
        } else {
          console.log('USUARIO NO EXISTE');
          console.log(profile);
          var user = new User({
              idsocial: profile.id,
              username: profile.name.givenName,
              email: profile.emails[0].value,
          });
          console.log("user" + user);
          user.save(function(err) {
              if(err){
                console.log(err);
                  return done(null, user);
              }
          });
      }
    });
  }
));




passport.use(new GitHubStrategy({
  clientID: socialKeys.GITHUB_CLIENT_ID,
  clientSecret:socialKeys.GITHUB_CLIENT_SECRET,
  callbackURL: socialKeys.GITHUB_CALLBACK,
},

function(request, accessToken, refreshToken, profile, done) {
  //console.log(profile);
  User.findOne({ 'idsocial' : profile.id }, function(err, user) {
    console.log(user);
    console.log(profile);
      if (err)
        return done(err);

      // if the user is found then log them in
      if (user) {
          console.log('USER EXISTS');
          return done(null, user);
      } else {
        console.log('USUARIO NO EXISTE');
        var user = new User({
            idsocial: profile.id,
            bio: profile.photos[0].value,
          
           
        });
        console.log("user" + user);
        user.save(function(err) {
            if(err){
              console.log(err);
                return done(null, user);
            }
        });
    }
  });
}
));
