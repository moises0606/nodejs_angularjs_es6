var router = require('express').Router();
var mongoose = require('mongoose');
var Events = mongoose.model('Events');
var User = mongoose.model('User');
var auth = require('../auth');

router.post('/', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(req.body.newevent.organizador.toString() === req.payload.id.toString()){
      let event = new Events(req.body.newevent);
      event.save().then((event) => {
          let idUser = user._id;
          User.update({_id: idUser},{$push:{favorites: ''+ event._id+''}}).then(() => {
              return res.json({event: event.toJSONFor()});
          });
      });
    } else {
      return res.sendStatus(403);
    }
  });
});

router.put('/removeevent', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(req.body.event.organizador.toString() === req.payload.id.toString()){

      let event = new Events(req.body.event);

      if (event.inscritos.length > 1) {
        let id = req.body.event._id;
        User.update({following:id},{$pull:{following:id}}).then(() => {
          User.update({favorites:id},{$pull:{favorites:id}}).then(() => {
            event.remove(id=event._id).then(() => {
              return res.send(true);
            });
          });
        });
      }else {
        let id = req.body.event._id;
        User.update({favorites:id},{$pull:{favorites:id}}).then(() => {
          event.remove(id=event._id).then(() => {
              return res.send(true);
          });
        });
      }
    } else {
      return res.sendStatus(403);
    }
  });
});

module.exports = router;