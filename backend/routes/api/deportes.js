var router = require('express').Router();
var mongoose = require('mongoose');
var Deportes = mongoose.model('Deportes');

router.get('/', function(req, res, next) {
  Deportes.find().then(function(deportes){
  return res.json({deportes: deportes});
}).catch(next);
});

module.exports = router;